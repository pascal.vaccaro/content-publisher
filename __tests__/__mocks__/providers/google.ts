export const googleMock = {
  id: "9",
  name: "google",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rss",
  publicURL: "",
  clientId: "",
  clientSecret: "",
  registerURI: "https://podcasts.google.com/search/",
  authorizationURI: "",
  publishURI: "https://pubsubhubbub.appspot.com/",
  uploadURI: "",
  scope: [""]
} as const;
