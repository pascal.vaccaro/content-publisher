import FormData from "form-data";
import got from "got";
import { RSSDistributor } from "../typings";
import { ITunes } from "./itunes";

export class GooglePodcast extends ITunes implements RSSDistributor {
  name = "google";
  type = "rss" as const;
  publicURL = "https://podcasts.google.com/";
  registerURI = "";
  authorizationURI = "";
  publishURI = "https://pubsubhubbub.appspot.com/";
  uploadURI = "";
  scope = [""];
  clientId = "test.google.client_id";

  async check(): Promise<boolean> {
    const publishURL = new URL(this.publishURI);
    const form = new FormData();
    form.append("hub.mode", "publish");
    form.append("hub.url", this.redirectURI);
    await got.post(publishURL.toString(), { form });
    return true;
  }

  async register(): Promise<string> {
    const registerURL = new URL(this.publicURL);
    if (process.env.NODE_ENV !== "production") registerURL.searchParams.set("client_id", this.clientId);
    return registerURL.toString();
  }
}
