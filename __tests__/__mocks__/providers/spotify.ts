export const spotifyMock = {
  id: "4",
  name: "spotify",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rss",
  publicURL: "https://podcasters.spotify.com/",
  clientId: "",
  clientSecret: "",
  registerURI: "https://accounts.spotify.com/authorize",
  authorizationURI: "https://accounts.spotify.com/api/token",
  publishURI: "",
  uploadURI: "",
  scope: ["user-read-private", "user-library-modify", "user-library-read"]
} as const;
