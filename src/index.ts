import * as providers from "./providers";

export const initProvider = (name: string, initState: string) => {
  if (!(name in providers)) throw new Error(`No provider found with name ${name}`);
  const Publisher = providers[name as keyof typeof providers];
  return new Publisher(initState);
};

export const listProviders = async (userId: string) =>
  Promise.all(
    Object.values(providers)
      .map((Publisher) => new Publisher(userId))
      .filter(publisher => publisher?.isRegistered)
      .map((publisher) =>
        publisher.getUserCredentials(userId, publisher.refresh.bind(publisher)).then(
          (creds) =>
            ({
              selectable: Boolean(creds && creds.accessToken && creds.expiresAt > new Date().getTime()),
              hasPages: typeof publisher["getPagesList"] === "function",
              ...publisher.json,
            } as const)
        )
      )
  );
