// import { SecretsManagerClient, CancelRotateSecretCommand } from "@aws-sdk/client-secrets-manager";
// const client = new SecretsManagerClient({ region: "eu-west1" });
import { readFile, writeFile } from "fs/promises";

const FAKE_SECRET = "secret.";
const getFakeSecret = async (name: string) => {
  try {
    const json = await readFile(`/home/pascal/workspace/etx/etx-content-publisher/src/lib/${name}.json`);
    return JSON.parse(json.toString());
  } catch (err) {
    if (process.env.NODE_ENV !== "production")
      return {
        SecretString: `${FAKE_SECRET}${JSON.stringify({
          accessToken: name + "-access_token",
          refreshToken: name + "-refresh_token",
          expiresAt: new Date().getTime() + 3600 * 1000,
        })}`,
        Name: name,
      };
  }
  return {};
};

export const getSecret = async (name: string) => {
  const { SecretString: secret } = await getFakeSecret(name);
  // const { SecretString: secret } = await client.getSecretValue({ SecretId: name });
  const value = secret.startsWith(FAKE_SECRET) ? secret.slice(FAKE_SECRET.length) : secret;
  try {
    return JSON.parse(value) ?? {};
  } catch (e) {
    return typeof value === "string" ? value : "";
  }
};

export const setSecret = async (name: string, value: Record<string, unknown>) => {
  const params = {
    Name: name,
    SecretString: JSON.stringify(value),
    Description: `Credentials for ${name}`,
    ClientRequestToken: process.env.AWS_CLIENT_REQUEST_TOKEN,
  };
  // await client.createSecret(params)
  await writeFile(
    `/home/pascal/workspace/etx/etx-content-publisher/src/lib/${name}.client_secret.json`,
    JSON.stringify(params),
    { flag: "w+" }
  );
};
