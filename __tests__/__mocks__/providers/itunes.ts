export const itunesMock = {
  id: "1",
  name: "itunes",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rss",
  publicURL: "https://podcastsconnect.apple.com/login?targetUrl=%2F",
  clientId: "",
  clientSecret: "",
  registerURI: "https://podcastsconnect.apple.com/login?targetUrl=%2F",
  authorizationURI: "",
  publishURI: "",
  uploadURI: "",
  scope: [""]
} as const;
