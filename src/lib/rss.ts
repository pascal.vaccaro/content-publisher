import { writeFile } from "fs/promises";
import Stream from "stream";
// import { S3Client PutObjectCommand } from "@aws-sdk/client-s3";
// const client = new S3Client({ region: "eu-west1" });

export const sendRSSFeed = async (name: string, body: string): Promise<string> => {
  const stream = new Stream();
  stream.pipe = (dest) => {
    dest.write(body);
    return dest;
  };
  const params = {
    Bucket: process.env.AWS_S3_FEED_BUCKET_NAME,
    Key: name,
    Body: stream,
  };
  await writeFile(`/home/pascal/workspace/etx/etx-content-publisher/src/${name.split("/")[0]}.xml`, body, { flag: "w" });
  // const result = await client.send(new PutObjectCommand(params));

  return "done";
};

type NewFeed = {
  title: string;
  description: string;
  language: string;
  link: string;
  image: string;
  category: string[];
  explicit?: boolean;
  author: string;
  owner: string;
};
type FeedOptions = {
  pubsub?: string;
};

export const createRSSFeed = (feed: NewFeed, opts: FeedOptions = {}) => {
  const pubDate = new Date()["toGMTString"]();
  return `<?xml version="1.0"?>
  <rss version="2.0" xmlns:g="http://base.google.com/ns/1.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
      <title>
        ${feed.title}
      </title>
      ${
        opts.pubsub
          ? `
      <atom:link href="${opts.pubsub}" rel="hub" />
      `
          : ""
      }
      <link${opts.pubsub ? ` href="${opts.pubsub}" rel="hub"` : ""}>
        ${feed.link}
      </link>
      <description>
        ${feed.description}
      </description>
      ${toCategories(feed.category)}
      ${
        feed.image
          ? `
      <itunes:image>
        <link>${feed.link}</link>
        <url>${feed.image}</url>
        <title>${feed.title}</title>
        <description>${feed.description}</description>
      </itunes:image>
      <image>
        <link>${feed.link}</link>
        <url>${feed.image}</url>
        <title>${feed.title}</title>
        <description>${feed.description}</description>
      </image>
      `
          : ""
      }
      <language>${feed.language}</language>
      <pubDate>${pubDate}</pubDate>
      <itunes:explicit>${feed.explicit ?? false}</itunes:explicit>
      <author>${feed.author}</author>
      <itunes:author>${feed.author}</itunes:author>
      <itunes:owner>${feed.author}</itunes:owner>
    </channel>
  </rss>
  `.trim();
};

type FeedItem = {
  title: string;
  description: string;
  author: string;
  category: string[];
  content: {
    size: number;
    type: string;
    url: string;
  };
  guid: string;
  link: string;
  duration?: number;
  explicit?: boolean;
  image?: string;
};
export const createRSSItem = (item: FeedItem) => {
  const pubDate = new Date()["toGMTString"]();
  return `<item>
      <title>${item.title}</title>
      <author>${item.author}</author>
      ${toCategories(item.category)}
      <description>${item.description}</description>
      <enclosure length="${item.content.size}" type="${item.content.type}" url="${item.content.url}" />
      <guid>${item.guid}</guid>
      <link>${item.link}</link>
      <pubDate>${pubDate}</pubDate>
      ${item.duration ? `<itunes:duration>${item.duration}</itunes:duration>` : ""}
      <itunes:explicit>${item.explicit ?? false}</itunes:explicit>
      ${item.image ? `<itunes:image href="${item.image}" />` : ""}
    </item>
  `.trim();
};

export const mergeItemWithFeed = (item: FeedItem, feed: string) => {
  const pos = feed.indexOf("</channel>");
  return `${feed.slice(0, pos)}
    ${createRSSItem(item)}
    ${feed.slice(pos)}`.trim();
};

const toCategories = (categories: string[]) =>
  categories.map(
    (category) => `
    <category>${category}</category>
    <itunes:category>${category}</itunes:category>
    `
  );
