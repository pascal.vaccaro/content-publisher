export * from "./amazon";
export * from "./deezer";
export * from "./facebook";
export * from "./google";
export * from "./instagram";
export * from "./itunes";
export * from "./linkedin";
export * from "./spotify";
export * from "./tiktok";
export * from "./twitter";
export * from "./youtube";
