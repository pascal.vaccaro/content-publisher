import got, { OptionsOfJSONResponseBody } from "got";
import FormData from "form-data";
import { Credentials, ETXStudioContent, RESTDistributor } from "../typings";
import { Distributor } from "./distributor";

type TwitterCredentials = {
  token_type: string;
  access_token: string;
  refresh_token?: string;
};

export class Twitter extends Distributor implements RESTDistributor {
  name = "twitter";
  type = "rest" as const;
  registerURI = "https://twitter.com/i/oauth2/authorize";
  authorizationURI = "https://api.twitter.com/2/oauth2/token";
  publishURI = "https://api.twitter.com/2/tweets";
  uploadURI = "https://upload.twitter.com/1.1/media/upload.json";
  scope = ["users.read", "tweet.read", "tweet.write", "offline.access"];
  clientId = "VV9rSFEyV0pyNS1JWGhYdTlsTWU6MTpjaQ";

  async upload?(file: ArrayBuffer, type: string): Promise<string> {
    const uploadURI = new URL(this.uploadURI);
    uploadURI.searchParams.set("command", "INIT");
    uploadURI.searchParams.set("total_bytes", `${file.byteLength}`);
    uploadURI.searchParams.set("media_type", type);

    const { media_id_string: mediaId } = await got.post(uploadURI.toString()).json<{ media_id_string: string }>();

    const append = async (index = 0) => {
      const body = new FormData();
      const end = 5e6 * (index + 1);
      const chunk = file.slice(index, end);
      body.append("command", "APPEND");
      body.append("media_id", mediaId);
      body.append("segment_index", index);
      body.append("media", chunk);
      const req = await this.authenticate(this.uploadURI, {
        body,
        headers: { "Content-Type": "multipart/form-data" },
      });
      await got.post(...req);
      if (chunk.byteLength === 5e6) return append(index + 5e6);
    };

    await append();
    const finalize = await this.authenticate(this.uploadURI, {
      form: {
        command: "FINALIZE",
        media_id: mediaId,
      },
    });
    await got.post(...finalize);
    return mediaId;
  }

  async register(): Promise<string> {
    const loginPage = new URL(this.registerURI);
    loginPage.searchParams.set("response_type", "code");
    loginPage.searchParams.set("client_id", this.clientId);
    loginPage.searchParams.set("redirect_uri", this.redirectURI);
    loginPage.searchParams.set("scope", this.scope.join(" "));
    loginPage.searchParams.set("state", this.state);
    loginPage.searchParams.set("code_challenge", "challenge");
    loginPage.searchParams.set("code_challenge_method", "plain");
    return loginPage.toString();
  }

  async authorize(code: string, userId: string = this.state): Promise<void> {
    const authURL = new URL(this.authorizationURI);
    const { clientId, clientSecret } = await this.getAppCredentials();
    const form = {
      client_id: clientId,
      client_secret: clientSecret,
      redirect_uri: this.redirectURI,
      grant_type: "authorization_code",
      code,
      code_verifier: "challenge",
    };
    const twCredentials = await got.post(authURL.toString(), { form }).json<TwitterCredentials>();
    await this.setUserCredentials({
      userId,
      distributor: this.name,
      accessToken: twCredentials.access_token,
      refreshToken: twCredentials.refresh_token,
      expiresAt: 0,
    });
  }

  async authenticate(url: string | URL, options: OptionsOfJSONResponseBody = {}): Promise<[URL, OptionsOfJSONResponseBody]> {
    const creds = await this.getUserCredentials(this.state, this.refresh.bind(this));
    if (!options.headers) options.headers = {};
    options.headers.Authorization = `Bearer ${creds.accessToken}`;
    return [new URL(url), options];
  }

  async refresh(token: string, userId = this.state): Promise<Credentials> {
    const authURL = new URL(this.authorizationURI);
    const { clientId } = await this.getAppCredentials();
    const form = {
      client_id: clientId,
      refresh_token: token,
      grant_type: "refresh_token",
    };
    const twCredentials = await got.post(authURL.toString(), { form }).json<TwitterCredentials>();
    return {
      userId,
      distributor: this.name,
      accessToken: twCredentials.access_token,
      refreshToken: twCredentials.refresh_token,
      expiresAt: 0,
    };
  }

  async publish(content: ETXStudioContent): Promise<string> {
    const { text, ...medias } = content;
    const publishURI = new URL(this.publishURI);
    const json = { text };

    const mediaIds = await Promise.all(
      Object.entries(medias)
        .filter(([key]) => key === "audio" || key === "video")
        .map(async ([key, value]) => {
          const type = key === "audio" ? "audio/mp3" : "video/mp4";
          const file = (await got(value)).rawBody;
          return this.upload(file, type);
        })
    );
    if (mediaIds.length) json["media"] = { media_ids: mediaIds };
    const [url, opts] = await this.authenticate(publishURI, { json });
    const { data } = await got.post(url, opts as OptionsOfJSONResponseBody).json<{ data: { id: string } }>();
    return data.id;
  }
}
