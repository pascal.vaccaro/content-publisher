import got, { OptionsOfJSONResponseBody } from "got";
import FormData from "form-data";
import { Credentials, ETXStudioContent, RSSDistributor } from "../typings";
import { Distributor } from "./distributor";
import { ITunes } from "./itunes";

type SpotifyCredentials = {
  access_token: string;
  refresh_token: string;
  expires_in: number;
  token_type: string;
};
export class Spotify extends ITunes implements RSSDistributor {
  name = "spotify";
  type = "rss" as const;
  publicURL = "https://podcasters.spotify.com/";
  registerURI = "https://accounts.spotify.com/authorize";
  authorizationURI = "https://accounts.spotify.com/api/token";
  publishURI = "";
  scope = ["user-read-private", "user-library-modify", "user-library-read"];
  clientId = "b76c4bd81183465ba2c2876405619cf3";

  async check(content: ETXStudioContent): Promise<boolean> {
    throw new Error("Method not implemented.");
  }

  async register(): Promise<string> {
    const loginPage = new URL(this.registerURI);
    loginPage.searchParams.set("response_type", "code");
    loginPage.searchParams.set("client_id", this.clientId);
    loginPage.searchParams.set("redirect_uri", this.redirectURI);
    loginPage.searchParams.set("scope", this.scope.join(" "));
    loginPage.searchParams.set("state", this.state);
    return loginPage.toString();
  }

  async authorize(code: string, userId: string = this.state): Promise<void> {
    const authURL = new URL(this.authorizationURI);
    const { clientId, clientSecret } = await this.getAppCredentials();
    const form = new FormData();
    form.append("redirect_uri", this.redirectURI);
    form.append("code", code);
    form.append("grant_type", "authorization_code");

    const headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${Buffer.from([clientId, clientSecret].join(":"), "base64").toString("base64")}`,
    };
    const spCredentials = await got.post(authURL.toString(), { form, headers }).json<SpotifyCredentials>();

    await this.setUserCredentials({
      distributor: this.name,
      userId,
      accessToken: spCredentials.access_token,
      refreshToken: spCredentials.refresh_token,
      expiresAt: new Date().getTime() + spCredentials.expires_in * 1000,
    });
  }

  async authenticate(
    url: string | URL,
    options: OptionsOfJSONResponseBody = {}
  ): Promise<[URL, OptionsOfJSONResponseBody]> {
    const creds = await this.getUserCredentials(this.state, this.refresh.bind(this));
    options.headers = {
      ...options.headers,
      Authorization: `Bearer ${creds.accessToken}`,
    };
    return [new URL(url.toString()), options];
  }

  async refresh(token: string, userId = this.state): Promise<Credentials> {
    const authURL = new URL(this.authorizationURI);
    const form = new FormData();
    const { clientId, clientSecret } = await this.getAppCredentials();
    form.append("refresh_token", token);
    form.append("grant_type", "refresh_token");

    const headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${Buffer.from([clientId, clientSecret].join(":"), "base64").toString("base64")}`,
    };
    const spCredentials = await got.post(authURL.toString(), { form, headers }).json<SpotifyCredentials>();

    return {
      distributor: this.name,
      userId,
      accessToken: spCredentials.access_token,
      refreshToken: spCredentials.refresh_token ?? token,
      expiresAt: new Date().getTime() + spCredentials.expires_in * 1000,
    };
  }
}
